/////////////////////////////////////////////////////////////
// Imports
/////////////////////////////////////////////////////////////
// Twit
var Twit = require('twit')
var T = new Twit({
    consumer_key:         'VR8hZm2sSt9RM51tHjUaIJ2ua'
    , consumer_secret:      'XbEEZlHPdpk1oUCEBJvsYXN5ZHUgqAbhs3bPO6LSABVcYXwsfs'
    , access_token:         '2506218470-AuXI7Xfub7GnEZhsZ5TFmDXLhBA9fAL7dQQIRzg'
    , access_token_secret:  'zjdUeUYU3fqc8FFB3SfyPxf7IEcAQh6tcBeN5T8sRpOIw'
})
// Filter the twitter public stream by the word 'banana'.
var stream = T.stream('statuses/filter', { track: '#banana' })
// Pi Blaster
var piblaster = require("pi-blaster.js");
// Pi GPIO
var gpio=require("pi-gpio");
// Priority Queue
var PriorityQueue = require('priorityqueuejs');
var queue = new PriorityQueue(function(a,b){return -1;});
// Hash Map
var map = require('./character_hashmap').init_hashmap();
// Stepper Motor Steps to perform
var steps;
var position = 0;
/////////////////////////////////////////////////////////////
// Operational Constants
/////////////////////////////////////////////////////////////
var servo = 0;
var stepper = 1;
/////////////////////////////////////////////////////////////
// Functions
/////////////////////////////////////////////////////////////
// Open Ports
gpio.open(16,"output",function(err){});
gpio.open(18,"output",function(err){});

// Begin Main tick function
tick();

// Stream from twitter
stream.on('tweet', function (tweet) {
    var streamText = tweet.text;
    // Create Event Queue
    for ( var i = 0; i < streamText.length; i++ ) {
	var streamCharacter = streamText.charAt(i);
	queue.enq({device:servo});
	queue.enq({device:stepper, character:streamCharacter});
    }
});

// Process Tick
function tick() {
    // Event available, process event
    if (queue.size() > 0) {
	// Process Event Type
	var operation = queue.deq();
	switch (operation.device) {
	case servo:
	    // servo_press();
	    setTimeout(tick, 10);
	    break;
	case stepper:
	    set_steps(operation.character);
	    setTimeout(tick, 10000);
	    break;
	default:
	    setTimeout(tick, 500);
	    break;
	}
    } 
    // Tick Default
    else {
	setTimeout(tick, 1000);
    }
}

// Stepper Move
function stepper_move () {
    for(var i = 0; i < 65; i++){
	gpio.write(16, 1, {}); 
	gpio.write(16, 0, {}); 
    }
    steps--;
    if (steps >= 0) {
	setTimeout(stepper_move, 500);
    }
}

// Stepper Movement Distance
function set_steps (characterStep) {
    // If Valid Character
    if (map.has(characterStep)) {
	var selectorPosition = map.get(characterStep);
	var differential = selectorPosition - position;

	if (differential >= 0) {
	    set_direction("right");
	} else {
	    set_direction("left");
	}
	steps = Math.abs(differential);
	position = selectorPosition;
	console.log("char: " + characterStep + " pos: "  
		    + selectorPosition + " steps: " + steps + " diff: " + differential);
	stepper_move();
    }
}

// Set Stepper Direction
function set_direction (direction) {
    if (direction == "right") {
	gpio.write(18, 1, function() {});
    } 
    if (direction == "left") {
	gpio.write(18, 0, function() {});
    }
}

// Servo Move Key
function servo_press () {
    piblaster.setPwm(4, .3);
    setTimeout(function(){
	piblaster.setPwm(4, .075);
    }, 1000);
}
