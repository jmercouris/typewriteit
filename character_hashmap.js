var HashMap = require('hashmap').HashMap;
// Initialize Hashmap
function init_hashmap() {
    var map = new HashMap();
    map.set("a", 4);
    map.set("b", 22);
    map.set("c", 14);
    map.set("d", 12);
    map.set("e", 11);
    map.set("f", 16);
    map.set("g", 20);
    map.set("h", 24);
    map.set("i", 31);
    map.set("j", 28);
    map.set("k", 32);
    map.set("l", 36);
    map.set("m", 30);
    map.set("n", 26);
    map.set("o", 35);
    map.set("p", 39);
    map.set("q", 3);
    map.set("r", 15);
    map.set("s", 8);
    map.set("t", 19);
    map.set("u", 27);
    map.set("v", 18);
    map.set("w", 7);
    map.set("x", 10);
    map.set("y", 23);
    map.set("z", 6);
    return map;
}
exports.init_hashmap = init_hashmap;
