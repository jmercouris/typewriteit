var text = "test test";
var index = 0;

iterateWithDelay();

function iterateWithDelay() {
    console.log(text.substring(index, index + 1));
    index = index + 1;
    setTimeout(iterateWithDelay, 250);
}
